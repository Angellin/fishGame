> [.gitlab-ci.yml](https://segmentfault.com/a/1190000010442764)文件被用来管理项目的runner 任务
> 该文件存放于项目仓库的根目录，它定义该项目如何构建。

### 工程化实践之webpack

### gitlab Page 部署流程
1.gitlab新建一个project
2.添加一个index.html文件
3.添加[.gitlab-ci.yml](https://segmentfault.com/a/1190000010442764)文件 注意代码格式
4.上传

### .gitlab-ci.yml 配置注意事项
> pages是一个特殊的job，用于将静态的内容上传到GitLab，可用于为您的网站提供服务。它有特殊的语法，因此必须满足以下两个要求：
>
> 任何静态内容必须放在public/目录下
> artifacts必须定义在public/目录下
>     pages:
>       stage: deploy
>       script:
>       - mkdir .public
>       - cp -r * .public
>       - mv .public public
>       artifacts:
>         paths:
>         - public
>       only:
>       - master

      在项目的设置下找到gitlab page 的链接地址
